// call Bertus Api
const dotenv = require('dotenv');
const request = require('request');

const bertusApi = function () {
  function get(endpointUrl, token) {
    var result = new Promise((resolve, reject) => {
      var options = {
        url: 'https://mybertus-api.bertus.com/integration/api/v1/' + endpointUrl,
        headers: {
          'Ocp-Apim-Subscription-Key': process.env.Ocp_Apim_Subscription_Key,
          'Authorization': 'Bearer ' + token
        },
        method: 'get'
      };

      request(options, function (err, resp, body) {
        if (!err && resp.statusCode === 200) {
          resolve(JSON.parse(body));
        }
        else {
          reject(resp.statusCode + ':' + resp.statusMessage + ' => ' + err);
        }
      })
    });

    return result;
  }

  function post(endpointUrl, token, data) {
    var result = new Promise((resolve, reject) => {
      var options = {
        url: 'https://mybertus-api.bertus.com/integration/api/v1/' + endpointUrl,
        headers: {
          'Ocp-Apim-Subscription-Key': process.env.Ocp_Apim_Subscription_Key,
          'Authorization': 'Bearer ' + token,
          'Content-Type': 'application/json'
        },
        method: 'post',
        body: JSON.stringify(data)
      };

      request(options, function (err, resp, body) {
        if (!err && resp.statusCode === 201) {
          resolve({
            location: resp.headers.location,
            data: JSON.parse(body)
          });
        }
        else {
          reject(
            {
              message: resp.statusCode,
              error: {
                status: resp.statusMessage,
                error: err
              }
            });
        }
      });
    });

    return result;
  }

  return {
    get: get,
    post: post
  };
}();

module.exports = bertusApi;