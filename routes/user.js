const express = require('express');
const passport = require('passport');
const ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn();
const router = express.Router();
const bertusApi = require('../bertusApi');

/* GET user profile. */
router.get('/', ensureLoggedIn, function(req, res, next) {
  bertusApi.get('accounts/' + req.user.accountNumber + '/shippingAddresses', req.user.token)
    .then(result => {
      res.render('user', {
        user: req.user,
        shippingAddresses: result
      });
    })
});

module.exports = router;
