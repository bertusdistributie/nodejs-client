const express = require('express');
const router = express.Router();
const bertusApi = require('../bertusApi');
const ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn();

/* GET search page. */
router.get('/', ensureLoggedIn, function (req, res, next) {

    console.log(JSON.stringify(req.query));

    filter = {
        artistStartsWith: req.query.artist,
        searchText: req.query.searchText
    }

    RenderSearchPage(filter, req.user, res);
});

router.post('/', ensureLoggedIn, function (req, res, next) {

    console.log(JSON.stringify(req.body));

    RenderSearchPage(req.body, req.user, res);
});

function RenderSearchPage(filter, user, res) {

    var requests = [
        bertusApi.post('accounts/' + user.accountNumber + '/searches',
            user.token,
            filter),
        bertusApi.get('typeahead?searchText=' + filter.searchText)
    ];

    Promise.all(requests)
        .then(responses => {
            var result = {
                searchText: filter.searchText,
                artistStartsWith: filter.artistStartsWith,
                result: responses[0].data,
                artists: responses[1].Artists,
                titles: responses[1].Titles,
                featuredArticles: []
            };

            console.log(responses[0].location.substring(42));

            bertusApi.get(responses[0].location.substring(42), user.token)
                .then(response => {
                    result.hits = response;

                    res.render('index',
                        result
                    );
                })
                .catch(error => {
                    console.log(error);
                    res.render('error', error);
                });
        })
        .catch(error => {
            console.log(error);
            res.render('error', error);
        });
}

module.exports = router;